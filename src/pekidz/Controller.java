package pekidz;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

public class Controller {

    @FXML
    private Label lblInstructions;
    @FXML
    private Label lblToneTxt;
    @FXML
    private Label lblTone;
    @FXML
    private Label   e1_00, e1_01, e1_02, e1_03, e1_04, e1_05, e1_06, e1_07, e1_08,
                    e1_09, e1_10, e1_11, e1_12, e1_13, e1_14, e1_15, e1_16,
                    e1_17, e1_18, e1_19, e1_20, e1_21, e1_22, e1_23, e1_24,
                    h2_00, h2_01, h2_02, h2_03, h2_04, h2_05, h2_06, h2_07, h2_08,
                    h2_09, h2_10, h2_11, h2_12, h2_13, h2_14, h2_15, h2_16,
                    h2_17, h2_18, h2_19, h2_20, h2_21, h2_22, h2_23, h2_24,
                    g3_00, g3_01, g3_02, g3_03, g3_04, g3_05, g3_06, g3_07, g3_08,
                    g3_09, g3_10, g3_11, g3_12, g3_13, g3_14, g3_15, g3_16,
                    g3_17, g3_18, g3_19, g3_20, g3_21, g3_22, g3_23, g3_24,
                    d4_00, d4_01, d4_02, d4_03, d4_04, d4_05, d4_06, d4_07, d4_08,
                    d4_09, d4_10, d4_11, d4_12, d4_13, d4_14, d4_15, d4_16,
                    d4_17, d4_18, d4_19, d4_20, d4_21, d4_22, d4_23, d4_24,
                    a5_00, a5_01, a5_02, a5_03, a5_04, a5_05, a5_06, a5_07, a5_08,
                    a5_09, a5_10, a5_11, a5_12, a5_13, a5_14, a5_15, a5_16,
                    a5_17, a5_18, a5_19, a5_20, a5_21, a5_22, a5_23, a5_24,
                    e6_00, e6_01, e6_02, e6_03, e6_04, e6_05, e6_06, e6_07, e6_08,
                    e6_09, e6_10, e6_11, e6_12, e6_13, e6_14, e6_15, e6_16,
                    e6_17, e6_18, e6_19, e6_20, e6_21, e6_22, e6_23, e6_24;

    @FXML
    public void initialize(){
        lblInstructions.setText("Hover over guitar neck to see tone positions.");
        lblToneTxt.setText("Tone:");
    }

    @FXML
    public void showBlank(){
        e1_00.setText("");
        e1_01.setText("");
        e1_02.setText("");
        e1_03.setText("");
        e1_04.setText("");
        e1_05.setText("");
        e1_06.setText("");
        e1_07.setText("");
        e1_08.setText("");
        e1_09.setText("");
        e1_10.setText("");
        e1_11.setText("");
        e1_12.setText("");
        e1_13.setText("");
        e1_14.setText("");
        e1_15.setText("");
        e1_16.setText("");
        e1_17.setText("");
        e1_18.setText("");
        e1_19.setText("");
        e1_20.setText("");
        e1_21.setText("");
        e1_22.setText("");
        e1_23.setText("");
        e1_24.setText("");
        h2_00.setText("");
        h2_01.setText("");
        h2_02.setText("");
        h2_03.setText("");
        h2_04.setText("");
        h2_05.setText("");
        h2_06.setText("");
        h2_07.setText("");
        h2_08.setText("");
        h2_09.setText("");
        h2_10.setText("");
        h2_11.setText("");
        h2_12.setText("");
        h2_13.setText("");
        h2_14.setText("");
        h2_15.setText("");
        h2_16.setText("");
        h2_17.setText("");
        h2_18.setText("");
        h2_19.setText("");
        h2_20.setText("");
        h2_21.setText("");
        h2_22.setText("");
        h2_23.setText("");
        h2_24.setText("");
        g3_00.setText("");
        g3_01.setText("");
        g3_02.setText("");
        g3_03.setText("");
        g3_04.setText("");
        g3_05.setText("");
        g3_06.setText("");
        g3_07.setText("");
        g3_08.setText("");
        g3_09.setText("");
        g3_10.setText("");
        g3_11.setText("");
        g3_12.setText("");
        g3_13.setText("");
        g3_14.setText("");
        g3_15.setText("");
        g3_16.setText("");
        g3_17.setText("");
        g3_18.setText("");
        g3_19.setText("");
        g3_20.setText("");
        g3_21.setText("");
        g3_22.setText("");
        g3_23.setText("");
        g3_24.setText("");
        d4_00.setText("");
        d4_01.setText("");
        d4_02.setText("");
        d4_03.setText("");
        d4_04.setText("");
        d4_05.setText("");
        d4_06.setText("");
        d4_07.setText("");
        d4_08.setText("");
        d4_09.setText("");
        d4_10.setText("");
        d4_11.setText("");
        d4_12.setText("");
        d4_13.setText("");
        d4_14.setText("");
        d4_15.setText("");
        d4_16.setText("");
        d4_17.setText("");
        d4_18.setText("");
        d4_19.setText("");
        d4_20.setText("");
        d4_21.setText("");
        d4_22.setText("");
        d4_23.setText("");
        d4_24.setText("");
        a5_00.setText("");
        a5_01.setText("");
        a5_02.setText("");
        a5_03.setText("");
        a5_04.setText("");
        a5_05.setText("");
        a5_06.setText("");
        a5_07.setText("");
        a5_08.setText("");
        a5_09.setText("");
        a5_10.setText("");
        a5_11.setText("");
        a5_12.setText("");
        a5_13.setText("");
        a5_14.setText("");
        a5_15.setText("");
        a5_16.setText("");
        a5_17.setText("");
        a5_18.setText("");
        a5_19.setText("");
        a5_20.setText("");
        a5_21.setText("");
        a5_22.setText("");
        a5_23.setText("");
        a5_24.setText("");
        e6_00.setText("");
        e6_01.setText("");
        e6_02.setText("");
        e6_03.setText("");
        e6_04.setText("");
        e6_05.setText("");
        e6_06.setText("");
        e6_07.setText("");
        e6_08.setText("");
        e6_09.setText("");
        e6_10.setText("");
        e6_11.setText("");
        e6_12.setText("");
        e6_13.setText("");
        e6_14.setText("");
        e6_15.setText("");
        e6_16.setText("");
        e6_17.setText("");
        e6_18.setText("");
        e6_19.setText("");
        e6_20.setText("");
        e6_21.setText("");
        e6_22.setText("");
        e6_23.setText("");
        e6_24.setText("");
    }

    @FXML
    public void showNoteE(){
        lblTone.setText("E");
        e1_00.setText("E");
        e1_12.setText("E");
        e1_24.setText("E");
        h2_05.setText("E");
        h2_17.setText("E");
        g3_09.setText("E");
        g3_21.setText("E");
        d4_02.setText("E");
        d4_14.setText("E");
        a5_07.setText("E");
        a5_19.setText("E");
        e6_00.setText("E");
        e6_12.setText("E");
        e6_24.setText("E");
    }

    @FXML
    public void showNoteF(){
        lblTone.setText("F");
        e1_01.setText("F");
        e1_13.setText("F");
        h2_06.setText("F");
        h2_18.setText("F");
        g3_10.setText("F");
        g3_22.setText("F");
        d4_03.setText("F");
        d4_15.setText("F");
        a5_08.setText("F");
        a5_20.setText("F");
        e6_01.setText("F");
        e6_13.setText("F");
    }

    @FXML
    public void showNoteFis(){
        lblTone.setText("F#");
        e1_02.setText("F#");
        e1_14.setText("F#");
        h2_07.setText("F#");
        h2_19.setText("F#");
        g3_11.setText("F#");
        g3_23.setText("F#");
        d4_04.setText("F#");
        d4_16.setText("F#");
        a5_09.setText("F#");
        a5_21.setText("F#");
        e6_02.setText("F#");
        e6_14.setText("F#");
    }

    @FXML
    public void showNoteG(){
        lblTone.setText("G");
        e1_03.setText("G");
        e1_15.setText("G");
        h2_08.setText("G");
        h2_20.setText("G");
        g3_00.setText("G");
        g3_12.setText("G");
        g3_24.setText("G");
        d4_05.setText("G");
        d4_17.setText("G");
        a5_10.setText("G");
        a5_22.setText("G");
        e6_03.setText("G");
        e6_15.setText("G");
    }

    @FXML
    public void showNoteGis(){
        lblTone.setText("G#");
        e1_04.setText("G#");
        e1_16.setText("G#");
        h2_09.setText("G#");
        h2_21.setText("G#");
        g3_01.setText("G#");
        g3_13.setText("G#");
        d4_06.setText("G#");
        d4_18.setText("G#");
        a5_11.setText("G#");
        a5_23.setText("G#");
        e6_04.setText("G#");
        e6_16.setText("G#");
    }

    @FXML
    public void showNoteA(){
        lblTone.setText("A");
        e1_05.setText("A");
        e1_17.setText("A");
        h2_10.setText("A");
        h2_22.setText("A");
        g3_02.setText("A");
        g3_14.setText("A");
        d4_07.setText("A");
        d4_19.setText("A");
        a5_00.setText("A");
        a5_12.setText("A");
        a5_24.setText("A");
        e6_05.setText("A");
        e6_17.setText("A");
    }

    @FXML
    public void showNoteB(){
        lblTone.setText("B");
        e1_06.setText("B");
        e1_18.setText("B");
        h2_11.setText("B");
        h2_23.setText("B");
        g3_03.setText("B");
        g3_15.setText("B");
        d4_08.setText("B");
        d4_20.setText("B");
        a5_01.setText("B");
        a5_13.setText("B");
        e6_06.setText("B");
        e6_18.setText("B");
    }

    @FXML
    public void showNoteH(){
        lblTone.setText("H");
        e1_07.setText("H");
        e1_19.setText("H");
        h2_00.setText("H");
        h2_12.setText("H");
        h2_24.setText("H");
        g3_04.setText("H");
        g3_16.setText("H");
        d4_09.setText("H");
        d4_21.setText("H");
        a5_02.setText("H");
        a5_14.setText("H");
        e6_07.setText("H");
        e6_19.setText("H");
    }

    @FXML
    public void showNoteC(){
        lblTone.setText("C");
        e1_08.setText("C");
        e1_20.setText("C");
        h2_01.setText("C");
        h2_13.setText("C");
        g3_05.setText("C");
        g3_17.setText("C");
        d4_10.setText("C");
        d4_22.setText("C");
        a5_03.setText("C");
        a5_15.setText("C");
        e6_08.setText("C");
        e6_20.setText("C");
    }

    @FXML
    public void showNoteCis(){
        lblTone.setText("C#");
        e1_09.setText("C#");
        e1_21.setText("C#");
        h2_02.setText("C#");
        h2_14.setText("C#");
        g3_06.setText("C#");
        g3_18.setText("C#");
        d4_11.setText("C#");
        d4_23.setText("C#");
        a5_04.setText("C#");
        a5_16.setText("C#");
        e6_09.setText("C#");
        e6_21.setText("C#");
    }

    @FXML
    public void showNoteD(){
        lblTone.setText("D");
        e1_10.setText("D");
        e1_22.setText("D");
        h2_03.setText("D");
        h2_15.setText("D");
        g3_07.setText("D");
        g3_19.setText("D");
        d4_00.setText("D");
        d4_12.setText("D");
        d4_24.setText("D");
        a5_05.setText("D");
        a5_17.setText("D");
        e6_10.setText("D");
        e6_22.setText("D");
    }

    @FXML
    public void showNoteDis(){
        lblTone.setText("D#");
        e1_11.setText("D#");
        e1_23.setText("D#");
        h2_04.setText("D#");
        h2_16.setText("D#");
        g3_08.setText("D#");
        g3_20.setText("D#");
        d4_01.setText("D#");
        d4_13.setText("D#");
        a5_06.setText("D#");
        a5_18.setText("D#");
        e6_11.setText("D#");
        e6_23.setText("D#");
    }
}