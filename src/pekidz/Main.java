package pekidz;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        Scene scene = new Scene(root, 900, 600);
        primaryStage.setScene(scene);
        scene.getStylesheets().add(Main.class.getResource("Style.css").toExternalForm());
        primaryStage.setTitle("NeckNotes");
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
